import React from 'react';
import logo from './logo.svg';
import './App.css';
import { directive } from '@babel/types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';


const useStyle = makeStyles((theme) => ({
  root: {
    '&>*': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));


function App() {

  const classes = useStyle();
   console.log(classes);
  return (
    <Container maxWidth='sm'>
      <div >
        <h1>
          Login
      </h1>
      </div>

      <form className={classes.root} noValidate autoComplete="off">

        <TextField id="outlined-basic" label="Email Address" variant="outlined" /><br />
        <TextField id="outlined-basic" label="Password" variant="outlined" />
      </form>

      <div>
        <Button name="" id="" variant="contained" type="button" value="Submit" color="primary">Submit</Button >
      </div>

    </Container>

  );
}

export default App;
